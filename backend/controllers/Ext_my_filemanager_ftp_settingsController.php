<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * Ftp file manager
 *
 *
 * @package MasihFathi
 * @subpackage Ftp file manager
 * @author Masih Fathi <masihfathi@gmail.com>
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */
class Ext_my_filemanager_ftp_settingsController extends Controller
{
    // the extension instance
    public $extension;

    // move the view path
    public function getViewPath()
    {
        return Yii::getPathOfAlias('ext-my-filemanager-ftp.backend.views.settings');
    }

    /**
     * Common settings for Amazon S3
     */
    public function actionIndex()
    {
        $request = Yii::app()->request;
        $notify = Yii::app()->notify;

        $model = new MyFilemanagerFtpExtCommon();
        $model->populate();

        if ($request->isPostRequest) {
            $model->attributes = (array)$request->getPost($model->modelName, array());
            if ($model->validate()) {
                $notify->addSuccess(Yii::t('app', 'Your form has been successfully saved!'));
                $model->save();
            } else {
                $notify->addError(Yii::t('app', 'Your form has a few errors, please fix them and try again!'));
            }
        }

        $this->setData(array(
            'pageMetaTitle' => $this->data->pageMetaTitle . ' | ' . Yii::t('ext_my_filemanager_ftp', 'Filemanager Ftp'),
            'pageHeading' => Yii::t('ext_my_filemanager_ftp', 'Filemanager Ftp'),
            'pageBreadcrumbs' => array(
                Yii::t('app', 'Extensions') => $this->createUrl('extensions/index'),
                Yii::t('ext_my_filemanager_ftp', 'Filemanager Ftp') => $this->createUrl('ext_my_filemanager_ftp_settings/index'),
            )
        ));

        $this->render('index', compact('model'));
    }
}