<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * Ftp file manager
 *
 *
 * @package MasihFathi
 * @subpackage Ftp file manager
 * @author Masih Fathi <masihfathi@gmail.com>
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

/**
 * This hook gives a chance to prepend content or to replace the default view content with a custom content.
 * Please note that from inside the action callback you can access all the controller view
 * variables via {@CAttributeCollection $collection->controller->data}
 * In case the content is replaced, make sure to set {@CAttributeCollection $collection->renderContent} to false
 * in order to stop rendering the default content.
 * @since 1.3.3.1
 */
$hooks->doAction('before_view_file_content', $viewCollection = new CAttributeCollection(array(
    'controller' => $this,
    'renderContent' => true,
)));

// and render if allowed
if ($viewCollection->renderContent) {
    $this->renderPartial('_tabs');
    /**
     * This hook gives a chance to prepend content before the active form or to replace the default active form entirely.
     * Please note that from inside the action callback you can access all the controller view variables
     * via {@CAttributeCollection $collection->controller->data}
     * In case the form is replaced, make sure to set {@CAttributeCollection $collection->renderForm} to false
     * in order to stop rendering the default content.
     * @since 1.3.3.1
     */
    $hooks->doAction('before_active_form', $collection = new CAttributeCollection(array(
        'controller' => $this,
        'renderForm' => true,
    )));

    // and render if allowed
    if ($collection->renderForm) {
        $form = $this->beginWidget('CActiveForm');
        ?>
        <div class="box box-primary borderless">
            <div class="box-header">
                <h3 class="box-title"><?php echo Yii::t('ext_my_filemanager_ftp', 'Ftp'); ?></h3>
            </div>
            <div class="box-body">
                <?php
                /**
                 * This hook gives a chance to prepend content before the active form fields.
                 * Please note that from inside the action callback you can access all the controller view variables
                 * via {@CAttributeCollection $collection->controller->data}
                 * @since 1.3.3.1
                 */
                $hooks->doAction('before_active_form_fields', new CAttributeCollection(array(
                    'controller' => $this,
                    'form' => $form
                )));
                ?>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'host'); ?>
                            <?php echo $form->textField($model, 'host', $model->getHtmlOptions('host')); ?>
                            <?php echo $form->error($model, 'host'); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'user'); ?>
                            <?php echo $form->textField($model, 'user', $model->getHtmlOptions('user')); ?>
                            <?php echo $form->error($model, 'user'); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'pass'); ?>
                            <?php echo $form->passwordField($model, 'pass', $model->getHtmlOptions('pass')); ?>
                            <?php echo $form->error($model, 'pass'); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'port'); ?>
                            <?php echo $form->textField($model, 'port', $model->getHtmlOptions('port')); ?>
                            <?php echo $form->error($model, 'port'); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <h4><?php echo Yii::t('ext_my_filemanager_ftp', 'Elfinder (file manager+ftp)'); ?></h4>
                <hr/>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'ftpFileManagerEnabled'); ?>
                            <?php echo $form->dropDownList($model, 'ftpFileManagerEnabled', $model->getYesNoOptions(), $model->getHtmlOptions('ftpFileManagerEnabled')); ?>
                            <?php echo $form->error($model, 'ftpFileManagerEnabled'); ?>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'fileManagerAssetPath'); ?>
                            <?php echo $form->textField($model, 'fileManagerAssetPath', $model->getHtmlOptions('fileManagerAssetPath')); ?>
                            <?php echo $form->error($model, 'fileManagerAssetPath'); ?>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'fileManagerAssetUrl'); ?>
                            <?php echo $form->textField($model, 'fileManagerAssetUrl', $model->getHtmlOptions('fileManagerAssetUrl')); ?>
                            <?php echo $form->error($model, 'fileManagerAssetUrl'); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <h4><?php echo Yii::t('ext_my_filemanager_ftp', 'Uploaded email templates (ftp)'); ?></h4>
                <hr/>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'ftpUploadTemplateEnabled'); ?>
                            <?php echo $form->dropDownList($model, 'ftpUploadTemplateEnabled', $model->getYesNoOptions(), $model->getHtmlOptions('ftpUploadTemplateEnabled')); ?>
                            <?php echo $form->error($model, 'ftpUploadTemplateEnabled'); ?>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'assetPathUploadTemplate'); ?>
                            <?php echo $form->textField($model, 'assetPathUploadTemplate', $model->getHtmlOptions('assetPathUploadTemplate')); ?>
                            <?php echo $form->error($model, 'assetPathUploadTemplate'); ?>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'assetUrlUploadTemplate'); ?>
                            <?php echo $form->textField($model, 'assetUrlUploadTemplate', $model->getHtmlOptions('assetUrlUploadTemplate')); ?>
                            <?php echo $form->error($model, 'assetUrlUploadTemplate'); ?>
                        </div>
                    </div>
                </div>
                <?php
                /**
                 * This hook gives a chance to append content after the active form fields.
                 * Please note that from inside the action callback you can access all the controller view variables
                 * via {@CAttributeCollection $collection->controller->data}
                 * @since 1.3.3.1
                 */
                $hooks->doAction('after_active_form_fields', new CAttributeCollection(array(
                    'controller' => $this,
                    'form' => $form
                )));
                ?>
                <div class="clearfix"><!-- --></div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit"
                            class="btn btn-primary btn-flat"><?php echo IconHelper::make('save') . Yii::t('app', 'Save changes'); ?></button>
                </div>
                <div class="clearfix"><!-- --></div>
            </div>
        </div>
        <?php
        $this->endWidget();
    }
    /**
     * This hook gives a chance to append content after the active form.
     * Please note that from inside the action callback you can access all the controller view variables
     * via {@CAttributeCollection $collection->controller->data}
     * @since 1.3.3.1
     */
    $hooks->doAction('after_active_form', new CAttributeCollection(array(
        'controller' => $this,
        'renderedForm' => $collection->renderForm,
    )));
}
/**
 * This hook gives a chance to append content after the view file default content.
 * Please note that from inside the action callback you can access all the controller view
 * variables via {@CAttributeCollection $collection->controller->data}
 * @since 1.3.3.1
 */
$hooks->doAction('after_view_file_content', new CAttributeCollection(array(
    'controller' => $this,
    'renderedContent' => $viewCollection->renderContent,
)));
