<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * Ftp file manager
 *
 *
 * @package MasihFathi
 * @subpackage Ftp file manager
 * @author Masih Fathi <masihfathi@gmail.com>
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

?>
<ul class="nav nav-tabs" style="border-bottom: 0px;">
    <li class="<?php echo $this->getAction()->getId() == 'index' ? 'active' : 'inactive'; ?>">
        <a href="<?php echo $this->createUrl('ext_my_filemanager_ftp_settings/index') ?>">
            <?php echo Yii::t('ext_my_filemanager_ftp', 'Common'); ?>
        </a>
    </li>
</ul>