<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * Ftp file manager
 *
 *
 * @package MasihFathi
 * @subpackage Ftp file manager
 * @author Masih Fathi <masihfathi@gmail.com>
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */
class MyFilemanagerFtpExt extends ExtensionInit
{
    // name of the extension as shown in the backend panel
    public $name = 'Ftp File Manager';

    // description of the extension as shown in backend panel
    public $description = 'Store assets in ftp';

    // current version of this extension
    public $version = '1.0';

    // minimum app version
    public $minAppVersion = '1.4.4';

    // the author name
    public $author = 'Masih Fathi';

    // author website
    public $website = 'https://www.avangemail.com';

    // contact email address
    public $email = 'masihfathi@gmail.com';

    // in which apps this extension is allowed to run
    public $allowedApps = array('backend', 'customer');

    // can this extension be deleted? this only applies to core extensions.
    protected $_canBeDeleted = true;

    // can this extension be disabled? this only applies to core extensions.
    protected $_canBeDisabled = true;

    // run the extension
    public function run()
    {
        // if php less than 5.5, stop.
        if (!version_compare(PHP_VERSION, '5.5', '>=')) {
            return;
        }

        Yii::import('ext-my-filemanager-ftp.common.models.*');

        if ($this->isAppName('backend')) {

            // handle all backend related tasks
            $this->backendApp();

        }
        // replace ftp storage with localhost elfinder
        Yii::app()->hooks->addFilter('ext_ckeditor_el_finder_options', array($this, '_FtpFileManager'));

        // add local uploaded template to ftp storage
        Yii::app()->hooks->addAction('email_template_upload_behavior_handle_upload_before_save_content', array($this, '_emailTemplateUpload'));
    }
    // handle all backend related tasks
    protected function backendApp()
    {
        // register the url rule to resolve the pages.
        Yii::app()->urlManager->addRules(array(
            array('ext_my_filemanager_ftp_settings/index', 'pattern' => 'extensions/my-filemanager-ftp-settings'),
            array('ext_my_filemanager_ftp_settings/<action>', 'pattern' => 'extensions/my-filemanager-ftp-settings/*'),
        ));

        // add the controllers
        Yii::app()->controllerMap['ext_my_filemanager_ftp_settings'] = array(
            'class' => 'ext-my-filemanager-ftp.backend.controllers.Ext_my_filemanager_ftp_settingsController',
            'extension' => $this,
        );


    }
    // callback for ext_ckeditor_el_finder_options hook
    public function _FtpFileManager($elfinderOpts)
    {
        // if the ftp file manager feature is disabled
        if ($this->getOption('ftpFileManagerEnabled', 'no') != 'yes') {
            return $elfinderOpts;
        }
        $host = $this->getOption('host');

        $user = $this->getOption('user');

        $pass = $this->getOption('pass');

        $port = $this->getOption('port');

        $filesPath = $this->getOption('fileManagerAssetPath');

        $filesUrl = $this->getOption('fileManagerAssetUrl');

        // if the parameters of ftp file manager are not set
        if (empty($host) || empty($user) || empty($pass) || empty($port) || empty($filesPath) || empty($filesUrl)) {
            Yii::log('host or user or pass or port or file manager asset path or file manager asset url are empty', CLogger::LEVEL_ERROR);
            return;
        }
        $elFinderVolumeFtpPath = Yii::getPathOfAlias('ext-ckeditor') . '/vendors/elfinder/elFinderVolumeFTP.class.php';
        if (file_exists($elFinderVolumeFtpPath )){
            require_once ( $elFinderVolumeFtpPath );
        }else {
            Yii::log('elFinderVolumeFTP.class.php file not found!', CLogger::LEVEL_ERROR);
            return;
        }


        $fileNameNoChars = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', ' ');

        $uploadAllow = array('image');

        $uploadDeny = array('all');

        $disabled = array('archive', 'extract', 'mkfile', 'rename', 'paste', 'put', 'netmount', 'callback', 'chmod', 'download');

        $ckeditor_extension = Yii::app()->extensionsManager->getExtensionInstance('ckeditor');

        if ($this->isAppName('customer') && $ckeditor_extension->getOption('enable_filemanager_customer') && Yii::app()->customer->getId() > 0) {
            // this is a customer requesting files.
            $customerFolderName = Yii::app()->customer->getModel()->customer_uid;


            $filesUrl = $filesUrl . '/customer/' . $customerFolderName;

            $filesPath .= '/customer';


            $conn = $this->_ftpConnect($host, $user, $pass, $port,10);

            if ($conn !== false) {
                // create directory (customer) if not exist

                if ($this->ftp_directory_exists($conn, $filesPath) === false) {
                    if (ftp_mkdir($conn, $filesPath) === false) {
                        Yii::log('Cant create customer directory(ftp)', CLogger::LEVEL_ERROR);
                        return;
                    }
                }

                $filesPath .= '/' . $customerFolderName;

                // create directory (customerFolderName) if not exist
                if ($this->ftp_directory_exists($conn, $filesPath) === false) {
                    if (ftp_mkdir($conn, $filesPath) === false) {
                        Yii::log('Cant create customerFolderName directory(ftp)', CLogger::LEVEL_ERROR);
                        return;
                    }
                }
                ftp_close($conn);
            }else{
                return;
            }
        }
        $elfinderOpts = array(
            'debug' => false,
            'bind' => array(
                'mkdir.pre mkfile.pre rename.pre' => array(
                    'Plugin.Sanitizer.cmdPreprocess'
                ),
                'upload.presave' => array(
                    'Plugin.Sanitizer.onUpLoadPreSave'
                )
            ),
            'plugin' => array(
                'Sanitizer' => array(
                    'enable' => true,
                    'targets' => $fileNameNoChars,
                    'replace' => '-'
                )
            ),
            'roots' => array(
                array(
                    'driver' => 'FTP',
                    'host' => $host,
                    'user' => $user,
                    'pass' => $pass,
                    'port' => $port,
                    'path' => $filesPath . '/',
                    'URL' => $filesUrl . '/',
                    'alias' => Yii::t('app', 'Home'),
                    'mode' => 'passive',
                    'uploadAllow' => $uploadAllow,
                    'uploadDeny' => $uploadDeny,
                    'uploadOverwrite' => false, // 1.6.6
                    'disabled' => $disabled,
                    'timeout'       => 10,
                    'owner'         => true,
                    // comment these two line if you don't want to store thumbnails
                    'tmbPath' => $elfinderOpts['roots'][0]['path'].'.tmb/',
                    'tmbURL'       => $elfinderOpts['roots'][0]['URL'].'.tmb/',
                    'dirMode'       => 0755,
                    'fileMode'      => 0644,
                    'dateFormat' => Yii::app()->locale->dateFormat,
                    'timeFormat' => Yii::app()->locale->timeFormat,
                    'attributes' => array(
                        // hide .tmb and .quarantine folders
                        array(
                            'pattern' => '/.(tmb|quarantine)/i',
                            'read' => false,
                            'write' => false,
                            'hidden' => true,
                            'locked' => false
                        ),
                    ),

                    'plugin' => array(
                        'Sanitizer' => array(
                            'enable' => true,
                            'targets' => $fileNameNoChars,
                            'replace' => '-'
                        )
                    )
                )
            )
        );
        return $elfinderOpts;
    }

    // callback for uploading template images to backend
    public function _emailTemplateUpload($params)
    {
        // if the feature email template upload behavior handle upload before save content is disabled
        if ($this->getOption('ftpUploadTemplateEnabled', 'no') != 'yes') {
            return;
        }
        $host = $this->getOption('host');

        $user = $this->getOption('user');

        $pass = $this->getOption('pass');

        $port = $this->getOption('port');

        $filesPath = $this->getOption('assetPathUploadTemplate');

        $filesUrl = $this->getOption('assetUrlUploadTemplate');

        // if the parameters of upload behavior are empty
        if (empty($host) || empty($user) || empty($pass) || empty($port) || empty($filesPath) || empty($filesUrl)) {
            Yii::log('host or user or pass or port or asset path upload template or asset url upload template are empty', CLogger::LEVEL_ERROR);
            return;
        }

        // for easier access
        $template = $params['template'];
        $originalContent = $params['originalContent'];
        $storagePath = $params['storagePath'];
        $storageDirName = $params['storageDirName'];
        $cdnSubdomain = $params['cdnSubdomain'];

        if ($cdnSubdomain || sha1($template->content) != sha1($originalContent)) {
            return;
        }
        $conn = $this->_ftpConnect($host, $user, $pass, $port,10);
        if ($conn !== false) {
            $searchReplace = array();
            $files = FileSystemHelper::readDirectoryContents($storagePath, true);
            if (!empty($template->screenshot)) {
                $files[] = Yii::getPathOfAlias('root') . $template->screenshot;
            }
            $p = $filesPath . '/' . $storageDirName;

            // if path to the gallery + Dir Name of template not available create it
            if ($this->ftp_directory_exists($conn, $p) === false) {

                if (ftp_mkdir($conn, $p) === false) {
                    Yii::log('Cant create folder(ftp)', CLogger::LEVEL_ERROR);
                    return;
                }
            }
            foreach ($files as $file) {
                $fileName = basename($file);
                $result = ftp_put($conn, $p . '/' . $fileName, $file, FTP_BINARY);
                if (!isset($result) || $result === false) {
                    Yii::log('can not upload file, local file path is:'. $file, CLogger::LEVEL_ERROR);
                    continue;
                }

                $src = Yii::app()->apps->getAppUrl('frontend', 'frontend/assets/gallery/' . $storageDirName . '/' . $fileName, true, true);

                $url = $filesUrl . '/' . $storageDirName . '/' . $fileName;

                $searchReplace[$src] = $url;

                if (!empty($template->screenshot) && basename($template->screenshot) == $fileName) {
                    $template->screenshot = $url;
                }
                // unset result of previous ftp file upload
                unset($result);
            }
            // close ftp connection
            ftp_close($conn);

            // replace local url to ftp url
            $template->content = str_replace(array_keys($searchReplace), array_values($searchReplace), $template->content);

            // try to delete all assets stored locally.
            FileSystemHelper::deleteDirectoryContents($storagePath);
        }

    }
    /**
     * function for connecting to ftp
     * @param $host
     * @param $user
     * @param $pass
     * @param int $port
     * @param int $timeout
     * @return resource
     */
    private function _ftpConnect($host, $user, $pass, $port = 21, $timeout = 10)
    {
        try {
            $con = ftp_connect($host, $port,$timeout);
            if (false === $con) {
                throw new Exception('Unable to connect to the FTP file manager server');
            }

            $loggedIn = ftp_login($con, $user, $pass);
            if (true === $loggedIn) {
                ftp_pasv($con, 'passive');
                return $con;
            } else {
                throw new Exception('Unable to log in to the FTP file manager server');
            }

        } catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * @param $ftp
     * @param $dir
     * @return bool| false if directory not exist and true if directory exists
     */
    private function ftp_directory_exists($ftp, $dir)
    {
        // Attempt to change directory, suppress warnings
        if (@ftp_chdir($ftp, $dir)) {
            return true;
        }
        // Directory does not exist
        return false;
    }

    // Add the landing page for this extension (settings/general info/etc)
    public function getPageUrl()
    {
        return Yii::app()->createUrl('ext_my_filemanager_ftp_settings/index');
    }
}
