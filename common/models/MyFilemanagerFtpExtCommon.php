<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * Ftp file manager
 *
 *
 * @package MasihFathi
 * @subpackage Ftp file manager
 * @author Masih Fathi <masihfathi@gmail.com>
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */
class MyFilemanagerFtpExtCommon extends FormModel
{
    protected $_extensionInstance;

    public $host = '';

    public $user = '';

    public $pass = '';

    public $port = '';

    public $ftpFileManagerEnabled = 'no';

    public $ftpUploadTemplateEnabled = 'no';

    public $fileManagerAssetPath = '';

    public $fileManagerAssetUrl = '';

    public $assetPathUploadTemplate = '';

    public $assetUrlUploadTemplate = '';

    public function rules()
    {
        $rules = array(
            array('host, user, pass, port,fileManagerAssetPath,fileManagerAssetUrl,assetPathUploadTemplate,assetUrlUploadTemplate', 'safe'),
            array('ftpFileManagerEnabled, ftpUploadTemplateEnabled', 'in', 'range' => array_keys($this->getYesNoOptions())),
        );
        return CMap::mergeArray($rules, parent::rules());
    }
    public function afterValidate()
    {
        if(!$this->hasErrors() && strlen($this->host) && strlen($this->user) && strlen($this->pass) && strlen($this->port) ){
            $connectionId = $this->_ftpConnect($this->host,$this->user,$this->pass,$this->port,10);
            if( $this->ftpFileManagerEnabled == 'yes' && $connectionId !== false){
                $this->changeDir($connectionId,$this->fileManagerAssetPath,'fileManagerAssetPath');
            }
            if( $this->ftpUploadTemplateEnabled == 'yes' && $connectionId !== false ){
                $this->changeDir($connectionId,$this->assetPathUploadTemplate,'assetPathUploadTemplate');
            }
        }
        parent::afterValidate();
    }
    /**
     * function for connecting to ftp
     * @param $host
     * @param $user
     * @param $pass
     * @param int $port
     * @param int $timeout
     * @return resource
     */
    private function _ftpConnect($host, $user, $pass, $port = 21, $timeout = 10)
    {
        try {
            $con = ftp_connect($host, $port ,$timeout);
            if (false === $con) {
                throw new Exception('Unable to connect');
            }

            $loggedIn = ftp_login($con, $user, $pass);
            if (true === $loggedIn) {
                ftp_pasv($con, 'passive');
                return $con;
            } else {
                throw new Exception('Unable to connect, user or password is not correct');
            }

        } catch (Exception $e) {
            $this->addError('host', $e->getMessage());
        }
    }
    /**
     * Change directory
     *
     * @param $connectionId
     * @param $directory
     * @param $attr
     * @return bool
     */
    private function changeDir($connectionId,$directory,$attr)
    {
        try {
            if(@ftp_chdir($connectionId, $directory))
                return true;
            else
                throw new Exception('Directory not exist');
        } catch(\Exception $e) {
            $this->addError($attr, $e->getMessage());
        }
    }
    public function attributeLabels()
    {
        $labels = array(
            'host' => Yii::t('ext_my_filemanager_ftp', 'Ftp host'),
            'user' => Yii::t('ext_my_filemanager_ftp', 'Ftp user'),
            'pass' => Yii::t('ext_my_filemanager_ftp', 'Ftp pass'),
            'port' => Yii::t('ext_my_filemanager_ftp', 'Ftp port'),
            'ftpFileManagerEnabled' => Yii::t('app', 'Enabled'),
            'ftpUploadTemplateEnabled' => Yii::t('app', 'Enabled'),
            'fileManagerAssetPath' => Yii::t('ext_my_filemanager_ftp', 'Asset base path'),
            'fileManagerAssetUrl' => Yii::t('ext_my_filemanager_ftp', 'Asset base url'),
            'assetPathUploadTemplate' => Yii::t('ext_my_filemanager_ftp', 'path to email gallery'),
            'assetUrlUploadTemplate' => Yii::t('ext_my_filemanager_ftp', 'url to email gallery'),
        );
        return CMap::mergeArray($labels, parent::attributeLabels());
    }

    public function attributePlaceholders()
    {
        $placeholders = array(
            'host' => '',
            'user' => '',
            'pass' => '',
            'port' => '',
            'fileManagerAssetPath' => '/domains/bucket.example.com/public_html/assets/files',
            'fileManagerAssetUrl' => 'https://bucket.example.com/assets/files',
            'assetPathUploadTemplate' => '/domains/bucket.example.com/public_html/assets/gallery',
            'assetUrlUploadTemplate' => 'https://bucket.example.com/assets/gallery',
        );
        return CMap::mergeArray($placeholders, parent::attributePlaceholders());
    }

    public function attributeHelpTexts()
    {
        $texts = array(
            'host' => Yii::t('ext_my_filemanager_ftp', 'Ftp host'),
            'user' => Yii::t('ext_my_filemanager_ftp', 'Ftp user'),
            'pass' => Yii::t('ext_my_filemanager_ftp', 'Ftp password'),
            'port' => Yii::t('ext_my_filemanager_ftp', 'Ftp port'),
            'ftpFileManagerEnabled' => Yii::t('app', 'Whether the ftp file manager feature is enabled'),
            'ftpUploadTemplateEnabled' => Yii::t('ext_my_filemanager_ftp', 'Whether the assets from email templates should be sent to Ftp service'),
            'fileManagerAssetPath' => Yii::t('ext_my_filemanager_ftp', 'Asset base path for ftp (elfinder)'),
            'fileManagerAssetUrl' => Yii::t('ext_my_filemanager_ftp', 'Asset base url for ftp (elfinder)'),
            'assetPathUploadTemplate' => Yii::t('ext_my_filemanager_ftp', 'path to the gallery folder'),
            'assetUrlUploadTemplate' => Yii::t('ext_my_filemanager_ftp', 'url to the gallery folder'),
        );
        return CMap::mergeArray($texts, parent::attributeHelpTexts());
    }

    public function save()
    {
        $extension = $this->getExtensionInstance();
        $attributes = array(
            'host',
            'user',
            'pass',
            'port',
            'ftpFileManagerEnabled',
            'ftpUploadTemplateEnabled',
            'fileManagerAssetPath',
            'fileManagerAssetUrl',
            'assetPathUploadTemplate',
            'assetUrlUploadTemplate'
        );
        foreach ($attributes as $name) {
            $extension->setOption($name, $this->$name);
        }
        return $this;
    }

    public function populate()
    {
        $extension = $this->getExtensionInstance();
        $attributes = array(
            'host',
            'user',
            'pass',
            'port',
            'ftpFileManagerEnabled',
            'ftpUploadTemplateEnabled',
            'fileManagerAssetPath',
            'fileManagerAssetUrl',
            'assetPathUploadTemplate',
            'assetUrlUploadTemplate'
        );
        foreach ($attributes as $name) {
            $this->$name = $extension->getOption($name, $this->$name);
        }
        return $this;
    }

    public function getExtensionInstance()
    {
        if ($this->_extensionInstance !== null) {
            return $this->_extensionInstance;
        }
        return $this->_extensionInstance = Yii::app()->extensionsManager->getExtensionInstance('my-filemanager-ftp');
    }

}
